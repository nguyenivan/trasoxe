from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin
from carmarket.models import Dealer, CarMake, CarYear, CarModel, CarStyle, StockCar, StockPhoto
admin.site.register(Dealer)
admin.site.register(CarMake)
admin.site.register(CarYear)
admin.site.register(CarModel)
admin.site.register(CarStyle)
class StockPhotoInline (AdminImageMixin, admin.TabularInline):
	model = StockPhoto
class StockCarAdmin(admin.ModelAdmin):
	list_display = ('thumbnail', 'style', 'used', 'code', 'dealer', 'description')
	inlines = (StockPhotoInline,)
admin.site.register(StockCar, StockCarAdmin)
