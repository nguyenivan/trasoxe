from django.conf.urls.defaults import patterns
from django.views.generic import ListView
from carmarket.models import StockCar

urlpatterns = patterns ('',
	(r'^thumbs/$', ListView.as_view(model=StockCar)),
)
