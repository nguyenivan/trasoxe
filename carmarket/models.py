# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from picklefield.fields import PickledObjectField
from sorl.thumbnail import ImageField, get_thumbnail

class Dealer(models.Model):
	CITY_CHOICES = (
		('HCM', _('Ho Chi Minh City')),
		('HN', _('Ha Noi')),
	)
	name = models.CharField(max_length = 256)
	description = models.TextField ()
	address = models.TextField()
	city = models.CharField(max_length = 3, choices = CITY_CHOICES, default = 'HCM' )
	def __unicode__(self):
		return self.name

class CarMake (models.Model):
	make = models.CharField (max_length = 50)
	def __unicode__(self):
		return self.make

class CarYear (models.Model):
	year = models.IntegerField()
	def __unicode__(self):
		return unicode(self.year)

class CarModel (models.Model):
	model = models.CharField(max_length = 50)
	make = models.ForeignKey(CarMake)
	def __unicode__(self):
		return '%s %s' % (self.make, self.model)

class CarStyle (models.Model):
	style = models.CharField(max_length = 50)
	model = models.ForeignKey(CarModel)
	year = models.ForeignKey(CarYear)
	options = PickledObjectField(blank = True, null = True)
	def __unicode__(self):
		return '%s %s %s' % (self.model, self.style, self.year)

class StockCar (models.Model):
	style = models.ForeignKey(CarStyle)
	used = models.BooleanField()
	code = models.CharField(max_length = 10)
	dealer = models.ForeignKey(Dealer)
	loaded_options = PickledObjectField(blank = True, null = True)
	description = models.TextField(blank = True, null = True)
	def __unicode__(self):
		return '%s %s, Code: %s, Dealer: %s' % ( ('Used' if self.used else 'New'), self.style, self.code, self.dealer)
	def thumbnail(self):
		if self.stockphoto_set.all():
			return u'<img src="%s" />' % get_thumbnail(self.stockphoto_set.all()[0].photo, '100x100', crop='center', quality=99).url
		else:
			return '(No Photo)'
	thumbnail.short_description = 'Thumb'
	thumbnail.allow_tags = True	
	
class StockPhoto(models.Model):
	photo = ImageField(upload_to = 'stock')
	car = models.ForeignKey(StockCar)
	title = models.CharField(max_length = 50, blank = True, null = True)
	
class StylePhoto(models.Model):
	photo = ImageField(upload_to = 'style')
	model = models.ForeignKey(CarStyle)
	title = models.CharField(max_length = 50, blank = True, null = True)
	
class ModelPhoto(models.Model):
	photo = ImageField(upload_to = 'model')
	model = models.ForeignKey(CarModel)
	title = models.CharField(max_length = 50, blank = True, null = True)

