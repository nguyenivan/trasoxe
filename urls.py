from django.conf.urls.defaults import patterns, include, url
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^i18n/', include('django.conf.urls.i18n')),
	url(r'^admin/', include(admin.site.urls)),
	url(r'^market/', include('carmarket.urls')),
	url(r'^history/', include('carhistory.urls')),
	url(r'^$', RedirectView.as_view(url='/history/')),
)

import os
if os.environ['SERVER_SOFTWARE'] == 'local':
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
