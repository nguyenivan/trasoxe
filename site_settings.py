# -*- coding: utf-8 -*-
#Invoke SERVER_SOFTWARE='Google App Engine' ./manage.py syncdb to run the local server in production mode
import os

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	# Running on production App Engine, so use a Google Cloud SQL database.
	DATABASES = {
		'default': {
			'ENGINE': 'google.appengine.ext.django.backends.rdbms',
			'INSTANCE': 'qhoach.com:bocation:instance1',
			'NAME': 'trasoxe',
		}
	}
else:
	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
			'NAME': 'trasoxe',					  # Or path to database file if using sqlite3.
			'USER': 'mysql',					  # Not used with sqlite3.
			'PASSWORD': 'nethouse',				  # Not used with sqlite3.
			'HOST': '',					  # Set to empty string for localhost. Not used with sqlite3.
			'PORT': '',					  # Set to empty string for default. Not used with sqlite3.
		}
	}

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	CACHES = {
		'default': {
			'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
			'TIMEOUT': 3600*24*2, # Two Weeks
			}
	}
	DEBUG = False
else:
	CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'bocation',
		}
	}
	DEBUG = True
	
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates')
)
if  os.getenv('SERVER_SOFTWARE', '')  == 'local':
	MEDIA_ROOT = os.path.join(PROJECT_ROOT, "media")
	STATIC_ROOT = os.path.join(PROJECT_ROOT, "static")
	MEDIA_URL = '/media/' 
	STATIC_URL = '/static/'
	ADMIN_MEDIA_PREFIX = '/static/admin/'
	STATICFILES_DIRS = (
		os.path.join(PROJECT_ROOT, "commonstatic"),
	)

else:
	MEDIA_URL = 'http://s3-ap-southeast-1.amazonaws.com/trasoxe/media/'
	STATIC_URL = '/static/'
	ADMIN_MEDIA_PREFIX = '/static/admin/'
	FILE_UPLOAD_HANDLERS = ('django.core.files.uploadhandler.MemoryFileUploadHandler',)
	FILE_UPLOAD_MAX_MEMORY_SIZE = 2621440


DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_ACCESS_KEY_ID = 'AKIAIWOS65YTK4ULDR4A'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'trasoxe'
AWS_S3_CUSTOM_DOMAIN = 's3-ap-southeast-1.amazonaws.com/trasoxe'
AWS_LOCATION = 'media'
AWS_QUERYSTRING_AUTH = True
AWS_S3_SECURE_URLS = False

TEMPLATE_CONTEXT_PROCESSORS = (
	'django.core.context_processors.request',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.core.context_processors.static',
	'django.contrib.auth.context_processors.auth',
	'django.contrib.messages.context_processors.messages',
)

LANGUAGES = (
	('en', u'English'),
	('vi', u'Tiếng Việt'),
)
