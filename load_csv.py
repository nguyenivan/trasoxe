# Full path and name to your csv file
import csv
import re
from carhistory.models import OwnershipHistoryEvent
from datetime import datetime
csv_filepathname=r"test/baominh01.csv"

platep = re.compile('(?P<citycode>\d{2})[-\s]*(?P<subcode>\w{1,2})[-\s]*(?P<number>\d{3,5})(\.(?P<add>\d{2}))?')
def normalize_plate(sequence):
	m = platep.match(sequence.lower())
	if m:
		gp=m.groupdict()
		result = '%s%s-%s%s' % (gp['citycode'],  gp['subcode'] , gp['number'], gp['add'] or '')
		if result.startswith('00'):
			return None
		else:
			return '%s%s-%s%s' % (gp['citycode'],  gp['subcode'] , gp['number'], gp['add'] or '') 
	else:
		return None
	
def load(csvfile=csv_filepathname, maxrow=-1, test = False):
	dataReader = csv.reader(open(csvfile), delimiter=',', quotechar='"')
	rownum = 0
	for row in dataReader:
		if rownum>0: # Ignore the header row, import everything else
			source = row[1] 
			mileage = row[11]
			comments = row[14]
			plate = row[17]
			date = row[23]
			if 'Claim Reporter Branch Code' in source:
				source = "Bao Minh"
			print "source:", source, 'mileage:', int(mileage), 'comments:', comments, 'plate:', normalize_plate(plate), 'date:', datetime.strptime(date, '%Y%m%d')
			if normalize_plate(plate) and not test:
				evt = OwnershipHistoryEvent()
				evt.date = datetime.strptime(date, '%Y%m%d')
				evt.mileage = int(mileage)
				evt.source = source
				evt.comments = comments
				evt.type = 'AC'
				evt.ownership = None
				evt.plate = normalize_plate(plate)
				evt.save()
				print "Saved!"
			print '-'*8
		if rownum == maxrow:
			break
		rownum += 1
		
