# -*- coding: utf-8 -*-
from django.http import HttpResponseBadRequest, HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from carhistory.utils import check_plate_or_vin, LicensePlateOrVinInvalid
from django.utils.translation import ugettext_lazy as _
from carhistory.models import Vin, OwnershipHistoryEvent
import traceback

def history(request):
	try:
		sequence = request.GET.get(u'sequence')
		etemp = 'carhistory/error.html'
		if not sequence: 
			return render_to_response(etemp, {'error':_('Input is empty')}, context_instance=RequestContext(request))
		try:
			checked = check_plate_or_vin(sequence.strip())
		except LicensePlateOrVinInvalid:
			return render_to_response(etemp, {'error':_('Plate number or VIN is not in valid format.')}, context_instance=RequestContext(request))
		if '-' in checked: #this is a plate
			events = OwnershipHistoryEvent.objects.filter(plate = checked)
			if events:
				return render_to_response('carhistory/byplate.html', { 'events': events } , context_instance=RequestContext(request))
		else : #this is a VIN
			vin = Vin.objects.filter(number = checked)
			if vin:
				return render_to_response('carhistory/byvin.html', { 'vin': vin[0] } , context_instance=RequestContext(request))
		return render_to_response(etemp, {'error':_('Vin or Plate number not found.')}, context_instance=RequestContext(request))
	except Exception :
		print(traceback.format_exc())