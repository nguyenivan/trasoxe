from django.conf.urls.defaults import patterns, url
from django.views.generic.simple import direct_to_template
urlpatterns = patterns('carhistory.views',
    url(r'^$', direct_to_template, {'template':'carhistory/home.html'}, name =  'carhistory_home' ),
    url(r'^result/$', 'history' , name =  'carhistory_result' ),
)