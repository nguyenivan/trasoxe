from exceptions import Exception
import re

#platep = re.compile('(?P<citycode>\d{2})[-\s]*(?P<subcode>\w{1,2})[-\s]*(?P<number>\d{3,5})')
platep = re.compile('(?P<citycode>\d{2})[-\s]*(?P<subcode>\w{1,2})[-\s]*(?P<number>\d{3,5})(\.(?P<add>\d{,2}))?')
vinp = re.compile('\w')

class LicensePlateOrVinInvalid(Exception):
	pass

def check_plate(sequence):
	#Check to see if the plate in format 50AZ1111
	m = platep.match(sequence.lower())
	if m:
		gp=m.groupdict()
		result = '%s%s-%s%s' % (gp['citycode'],  gp['subcode'] , gp['number'], gp['add'] or '')
		if result.startswith('00'):
			return None
		else:
			return '%s%s-%s%s' % (gp['citycode'],  gp['subcode'] , gp['number'], gp['add'] or '') 
	else:
		return None

def check_vin(sequence):
	r = vinp.findall(sequence)
	if r and len(r)==17:
		return ''.join(r)
	else:
		return None

def check_plate_or_vin(sequence):
	result = check_plate(sequence)
	if result: return result
	result = check_vin(sequence)
	if result: return result
	raise LicensePlateOrVinInvalid()