from django.db import models
from django.utils.translation import ugettext_lazy as _


class Vin (models.Model):
	number = models.CharField(max_length = 20)
	model = models.CharField(max_length = 50)
	def __unicode__(self):
		return '%s (%s)' % (self.number, self.model)
	
class CarOwnership (models.Model):
	vin = models.ForeignKey(Vin)
	where = models.CharField(max_length = 50, blank = True, null = True)
	purchase_date = models.DateField(blank = True, null = True)
	release_date = models.DateField(blank = True, null = True)
	mileage = models.IntegerField(blank = True, null = True, help_text = u'Est. mileage during ownership')
	age = models.IntegerField(blank = True, null = True)
	def __unicode__(self):
		return '%s purchased on %s' % (self.vin, self.purchase_date)
	def summary(self):
		result = self.purchase_date.strftime('%d/%m/%y')
		result = u'%s %s, %s' % (_('Transfered:'), result, self.where) if self.where else result

		result = u'%s, %s km' % (result, self.mileage) if self.mileage else result
		return result

class OwnershipHistoryEvent(models.Model):
	EVENT_TYPES = (
		('SV', _('Salvage')),
		('RB', _('Rebuilt')),
		('AC', _('Accident')),
		('TT', _('Title Change')),
	)
	date = models.DateField(help_text = u'Date on which history event occurred')
	mileage = models.IntegerField(blank = True, null = True, help_text = u'Mileage read')
	source = models.TextField(blank = True, null = True, help_text = u'Source of this information, with address')
	comments = models.TextField()
	type = models.CharField(max_length=2, choices = EVENT_TYPES, blank = True, null = True)
	ownership = models.ForeignKey(CarOwnership, blank = True, null = True)
	plate = models.CharField(max_length = 20, blank = True, null = True)
	def __unicode__(self):
		return '%s occurred on %s' % (self.ownership, self.date)		
	def date_km(self):
		result = self.date.strftime('%d/%m/%y')
		result = u'%s<br />%s km' % (result, self.mileage) if self.mileage else result
		return result
		


