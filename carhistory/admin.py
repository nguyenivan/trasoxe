from django.contrib import admin
from carhistory.models import Vin, OwnershipHistoryEvent, CarOwnership

class VinAdmin(admin.ModelAdmin):
	list_display = ('model', 'number')
admin.site.register(Vin, VinAdmin)

class OwnershipHistoryEventAdmin(admin.ModelAdmin):
	list_display = ('date', 'ownership', 'type', 'source', 'comments', 'mileage')
admin.site.register(OwnershipHistoryEvent, OwnershipHistoryEventAdmin)

class CarOwnershipAdmin(admin.ModelAdmin):
	list_display = ('vin', 'where', 'purchase_date', 'release_date', 'mileage', 'age')
admin.site.register(CarOwnership, CarOwnershipAdmin)
